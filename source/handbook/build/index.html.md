---
layout: markdown_page
title: "Build"
---

## Common links

* [Build issue tracker](https://gitlab.com/gitlab-org/omnibus-gitlab/issues). If
you need to submit sensitive issue, please use confidential issues.
* [Slack chat channel](https://gitlab.slack.com/archives/build)

## Team responsibility

Build is about how we ship GitLab, and making sure everyone can easily install,
update and maintain GitLab.

This means:

- Make Omnibus packages
- Maintain the official installations on our [installation page](https://about.gitlab.com/installation/)
- Make sure nightly builds are installed on dev.gitlab.org
- Keep the installation and download pages up to date and attractive
- Address community questions in the [omnibus-gitlab issue tracker](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/)
and mentions in GitLab CE/EE repositories on issues with `Build` label

### Projects

| Name | Location | Description |
| -------- | -------- |
| Omnibus Gitlab | [gitlab-org/omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab) | Build Omnibus packages with HA support for LTS versions of all major Linux operating systems such as Ubuntu, Debian, CentOS/RHEL, OpenSUSE, SLES |
| Docker GitLab image | [gitlab-org/omnibus-gitlab/docker](https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master/docker) | Build Docker images for GitLab CE/EE based on the omnibus-gitlab package |
| AWS images | [AWS marketplace](https://aws.amazon.com/marketplace/pp/B071RFCJZK?qid=1493819387811&sr=0-1&ref_=srh_res_product_title) | AWS image based on the omnibus-gitlab package |
| Azure images | [Azure marketplace](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/gitlab.gitlab-ee?tab=Overview) | Azure image based on the omnibus-gitlab package |
| Kubernetes helm charts | [charts/charts.gitlab.io](https://gitlab.com/charts/charts.gitlab.io) [Official Helm Charts](https://github.com/kubernetes/charts) | Application definitions for Kubernetes Helm based on the omnibus-gitlab package |
| Redhat Openshift | [Openshift template](https://gitlab.com/gitlab-org/omnibus-gitlab/docker/openshift-template.json) | Template for Openshift Origin based on the omnibus-gitlab package |
| Mesosphere DC/OS package | [Universe repository](https://github.com/mesosphere/universe/tree/version-3.x/repo/packages/G/gitlab) | Package for Mesosphere DC/OS based on omnibus-gitlab package |
| GitLab PCF tile | [gitlab.com/gitlab-pivotal](https://gitlab.com/gitlab-pivotal) | One click installation of GitLab in Pivotal Cloud Foundry based on omnibus-gitlab package |
| GitLab Terraform configuration | [gitlab-terraform](https://gitlab.com/gitlab-terraform) | Terraform configuration for various cloud providers |
| Omnibus GitLab Builder | [GitLab Omnibus Builder](https://gitlab.com/gitlab-org/gitlab-omnibus-builder) | Create environment containing build dependencies for the omnibus-gitlab package |

## How to work with Build

Everything that is done in GitLab will end up in the packages that are shipped
to users. While that sounds like the last link in the chain, it is one of the
most important ones. This means that informing the Build team of a change in an
early stage is crucial for releasing your feature. While last minute changes are
inevitable and can happen, we should strive to avoid them.

We expect every team to reach out to the Build team before scheduling a feature
in an upcoming release in the following cases:

* The change requires a new or an update on a gem with native extensions.
* The change requires a new or updated external software dependency.
  * Also when the external dependency has its own external dependencies.
* The change adds, modifies, or removes files that should be managed by
omnibus-gitlab. For example:
  * The change introduces new directories in the package.
* The change requires a new configuration file.
* The change requires a change in a previously established configuration.

To sum up the above list:

If you need to do `install`, `update`, `make`, `mkdir`, `mv`, `cp`, `chown`,
`chmod`, compilation or configuration change in any part of GitLab stack, you
need to reach out to the Build team for opinion as early as possible.

This will allow us to schedule appropriately the changes (if any) we have to make
to the packages.

If a change is reported late in the release cycle or not reported at all,
your feature/change might not be shipped within the release.

If you have any doubt whether your change will have an impact on the Build team,
don't hesitate to ping us in your issue and we'll gladly help.

TODO's

1. Installation page visuals https://gitlab.com/gitlab-com/www-gitlab-com/issues/1074
1. Lower the barrier of contributing to the Build team task
  * First goal, allow simpler creation of packages: https://gitlab.com/gitlab-org/omnibus-gitlab/issues/2234

## Internal team training

Every Build team member is responsible for creating a training session for the
rest of the team. These trainings will be recorded and available to the whole
team.

### Purpose

The purpose of team training is to introduce the work done to the rest of your team.
It also allows the rest of the team to easily transition into new features and projects
and prepare them for maintenance.

Training should be:

* Providing a high level overview of the invested work
* Describing the main challenges encountered during development
* Explaining the possible pit-falls and specificities

Training *should not* be:

* Replacement for documentation
* Replacement for writing down progress in issues
* Replacement for follow up issues

Simply put, the training is a summation of: notes taken in issues during development,
programming challenges, high level overview of written documentation. Your team
member should be able to take over the maintenance or build on top of your feature
with less effort after they have been part of the training.

*Note* Do not shy away from being technical in your training. You can ask yourself:
What would have been useful for me when I started working on this task? What
would have helped me be more efficient?

### Efficiency of the training

In order to see whether the training is efficient, Build lead will rotate team
members on projects where training was done. For example, if the feature
requires regular releases, the person who gave the training will be considered
a tutor. Different team member will follow the training and documentation and
will ask the original maintainer for help. The new person responsible is now
also responsible for improving the feature. They are now also responsible of
training other team members.

### FAQ

Q: Isn't this double work?
A: No. The training should be prepared while documenting the task.

Q: Won't this slow me down?
A: At the beginning, possibly. However, every hour of the training given will
multiple the value of it by the amount of team members.

Q: Isn't it more useful to let the team check out the docs and ask questions?
A: In an ideal world, possibly. However, everyone has a lot of tasks assigned
and they might not be able to go through the docs until they need to do something.
This might be months later and you, as a person who would give the training, might not
be able help efficiently anymore.

## Public by default

All work carried out by the Build team is public. Some exceptions apply:

* Work has possible security implications - If during the course of work security concerns are no longer valid,
it is expected for this work to become public.
* Work is done with a third party - Only when a third party requests that the work is not public.
* Work has financial implications - Unless financial details can be omitted from the work.

If you are unsure whether something needs to remain private, check with the team lead.

### Working on dev.gitlab.org

Some of the team work is carried out on our development server at `dev.gitlab.org`.
[Infrastructure overview document](https://docs.gitlab.com/omnibus/release/README.html#infrastructure) lists the reasons.

Unless your work is related to the security, all other work is carried out in projects on `GitLab.com`.

## Cloud Images

The process documenting the steps necessary to update the GitLab images available on
the various cloud providers is detailed on our [Cloud Image Process page](https://about.gitlab.com/cloud-images/).
