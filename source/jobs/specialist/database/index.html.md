---
layout: job_page
title: "Database Specialist"
---

## Responsibilities

At GitLab, the database specialist is a hybrid role: part developer, part
database expert. While the specialist will own the operational side of the
database (configuration changes, monitoring, upgrading the database, and
performance), a considerable part of the workload will be making changes to
GitLab itself. This role is dedicated to improving the database performance and
reducing load both by making changes to the database and optimizing application
code. The database specialist will straddle the line between operations and
development, working with one foot in both worlds. Past site reliability
engineers may succeed in this role.

As a member of our infrastructure team, you'll report to the Director of
Infrastructure. This role does include on-call rotations to respond to
GitLab.com availability incidents. More information on how our on-call works can
be found in our
[handbook](https://about.gitlab.com/handbook/on-call/#production-team-on-call-rotation).

## Requirements

* At least intermediate abilities in Ruby (preferred), Python, or other dynamic languages.
* Experience using frameworks such as Ruby on Rails, Django, Hibernate.
* Solid understanding of SQL.
* Significant experience running PostgreSQL in large production environments is a strict requirement.
* Demonstrated experience developing application logic.
* Deep understanding of the database, how it behaves, and the impact on the application as it scales.
* Significant experience working in a distributed production environment.
* Solid understanding of the internals of PostgreSQL.
* You share our [values](/handbook/#values), and work in accordance with those values.
* Excellent written and verbal English communication skills.

## Internships

We are currently not accepting any internships for this position.
