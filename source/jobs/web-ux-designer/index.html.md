---
layout: job_page
title: "Senior Web/UX Designer"
---

GitLab is looking to add a senior-level UX Designer to our Marketing team to help design, develop & maintain our website and other marketing properties. As the Marketing UX Designer you will work closely with product marketing, content marketing, and other members of the Marketing team. This role will be part of the Marketing Design team and report directly to the CMO.

## Responsibilities
* Lead UX, design, and front-end implementation of about.gitlab.com, emails and standalone landing pages.
* Develop & maintain a top notch user experience across all of these properties.
* Research and design concepts that increase engagement and conversion.
* Translate problems and new concepts into creative, intuitive, and data-driven web experiences.
* Define UX style guides and user flows to drive efficiency and consistency in our user experience.
* Build and establish a cohesive visual language.

## Requirements
* 6+ years experience specializing in web design, UX design, and front-end implementation.
* A professional portfolio demonstrating a strong background in web, UX, front-end, and visual design (candidates without a portfolio will not be considered).
* Expert-level understanding of responsive design and best practices.
* Proficient with HTML & CSS.
* Comfortable with JavaScript, as required by design tasks and front-end implementation.
* Experience running effective A/B tests.
* Exceptional knowledge of design tools (Sketch, Adobe CC, etc.).
* You use research and best practices to create, validate, and present your ideas to project stakeholders.
* Obsessive over the details and creating thoughtful and intuitive experiences.
* Able to iterate quickly and embrace feedback from many perspectives.
* Strong understanding of information architecture, interaction design, and user-centered design best practices.
* A track record of being self-motivated and delivering on time.
* Candidates in Americas timezones will be preferred.

## Nice-to-haves
* Able to pitch in on other design efforts; i.e. branding, illustration, swag, social ad campaigns, digital publications, etc.
* Experience working in a fully or partially remote company.

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a request for portfolios which will be reviewed by our hiring team
* Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a 60m minute meeting with our Designer
* Candidates will then be invited to schedule an interview with our VP of Marketing
* Finally, successful candidates may be asked to interview with our CEO
