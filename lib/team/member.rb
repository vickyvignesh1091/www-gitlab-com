module Gitlab
  module Homepage
    class Team
      class Member
        attr_reader :assignments

        def initialize(data)
          @data = data
          @assignments = []
        end

        def anchor
          twitter || gitlab || name.parameterize
        end

        def username
          @data.fetch('gitlab')
        end

        def involved?(project)
          roles.has_key?(project.key)
        end

        def roles
          @roles ||= Hash.new.tap do |hash|
            @data.fetch('projects', {}).each do |project, roles|
              Array(roles).each { |role| (hash[project] ||= []) << role }
            end
          end
        end

        def assign(project)
          roles[project.key].each do |role|
            @assignments << Team::Assignment.new(self, project, role)
          end
        end

        ##
        # Middeman Data File objects compatibiltiy
        #
        def method_missing(name, *args, &block)
          @data[name.to_s]
        end

        def self.all!
          @members ||= YAML.load_file('data/team.yml')

          @members.map do |data|
            self.new(data).tap { |member| yield member if block_given? }
          end
        end
      end
    end
  end
end
